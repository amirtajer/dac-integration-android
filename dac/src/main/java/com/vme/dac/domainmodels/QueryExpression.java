package com.vme.dac.domainmodels;

import java.util.List;

/**
 * Created by Amir Tajer on 3/13/18.
 */
public class QueryExpression {

    private String type;
    private List<Expression> expressions;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<Expression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public String toString() {
        return "QueryExpression{" +
                "type='" + type + '\'' +
                ", expressions=" + expressions +
                '}';
    }
}
