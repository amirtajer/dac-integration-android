package com.vme.dac.domainmodels;

import java.io.Serializable;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public class CreateLicenseResponse implements Serializable{

    private String requestId;
    private String requestEncKey;
    private String appLink;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestEncKey() {
        return requestEncKey;
    }

    public void setRequestEncKey(String requestEncKey) {
        this.requestEncKey = requestEncKey;
    }

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    @Override
    public String toString() {
        return "CreateLicenseResponse{" +
                "requestId='" + requestId + '\'' +
                ", requestEncKey='" + requestEncKey + '\'' +
                ", appLink='" + appLink + '\'' +
                '}';
    }
}
