package com.vme.dac.common;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public class DACError {
    public String code;         //e.g. BAD_REQUEST or NOT_FOUND
    public DACErrors text;       //more specific than code e.g. CAR_ALREADY_IN_DB
//    public Domain domain;       //the module that created this error e.g. Auto
    public String errorTitle;   //The title to show in the Error Alert Dialog box
    public String errorDescription;  //The message to show in the Error Alert Dialog Box


    public DACError(String code, DACErrors text) {//, Domain domain) {
        this.code = code;
        this.text = text;
//        this.domain = domain;
    }

    public DACError(String code){
        this.code = code;
    }

    public DACError(){}
}
