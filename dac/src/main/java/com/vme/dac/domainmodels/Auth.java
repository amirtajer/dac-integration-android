package com.vme.dac.domainmodels;

/**
 * Created by Amir Tajer on 3/13/18.
 */
public class Auth {

    private boolean returnId;
    private String subject;

    public boolean isReturnId() {
        return returnId;
    }

    public void setReturnId(boolean returnId) {
        this.returnId = returnId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Auth{" +
                "returnId=" + returnId +
                ", subject='" + subject + '\'' +
                '}';
    }
}
