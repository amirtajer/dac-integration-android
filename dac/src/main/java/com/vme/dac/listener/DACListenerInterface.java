package com.vme.dac.listener;

import com.vme.dac.common.Status;
import com.vme.dac.common.DACError;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public interface DACListenerInterface {
    void onSuccess (Status status, Object model);
    void onError (Status status, DACError error);
}
