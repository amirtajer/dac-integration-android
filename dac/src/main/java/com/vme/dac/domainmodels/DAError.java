package com.vme.dac.domainmodels;

/**
 * Created by Amir Tajer on 3/14/18.
 */
public class DAError {

    private boolean isRecoverable;

    public boolean isRecoverable() {
        return isRecoverable;
    }

    public void setRecoverable(boolean recoverable) {
        isRecoverable = recoverable;
    }

    @Override
    public String toString() {
        return "DAError{" +
                "isRecoverable=" + isRecoverable +
                '}';
    }
}
