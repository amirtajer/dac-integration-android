package com.vme.dac.domainmodels;

import java.io.Serializable;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public class CreateLicenseRequest implements Serializable{

    private QueryExpression queryExpression;
    private Auth auth;
    private String licenseNotificationUrl;
    private DisplayText displayText;
    private String defaultLang;
    private String state;

    public QueryExpression getQueryExpression() {
        return queryExpression;
    }

    public void setQueryExpression(QueryExpression queryExpression) {
        this.queryExpression = queryExpression;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public String getLicenseNotificationUrl() {
        return licenseNotificationUrl;
    }

    public void setLicenseNotificationUrl(String licenseNotificationUrl) {
        this.licenseNotificationUrl = licenseNotificationUrl;
    }

    public DisplayText getDisplayText() {
        return displayText;
    }

    public void setDisplayText(DisplayText displayText) {
        this.displayText = displayText;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "CreateLicenseRequest{" +
                "queryExpression=" + queryExpression +
                ", auth=" + auth +
                ", licenseNotificationUrl='" + licenseNotificationUrl + '\'' +
                ", displayText=" + displayText +
                ", defaultLang='" + defaultLang + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
