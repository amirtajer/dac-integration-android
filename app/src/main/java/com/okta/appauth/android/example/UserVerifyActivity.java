package com.okta.appauth.android.example;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.MainThread;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vme.dac.common.DACError;
import com.vme.dac.common.Status;
import com.vme.dac.domainmodels.UserDataRequest;
import com.vme.dac.domainmodels.UserDataResponse;
import com.vme.dac.listener.DACListenerInterface;
import com.vme.dac.services.DACAdapter;

public class UserVerifyActivity extends AppCompatActivity {

    private DACAdapter mDACAdapter;
    private UserDataResponse mUserDataResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_verify);

        mDACAdapter = DACAdapter.getInstance(this);

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        String license = data.getQueryParameter("license");
        if(license != null) {
            retrieveUserData(license);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        displayVerifiedUserInfo();
    }

    @MainThread
    private void displayLoading(String message) {
        findViewById(R.id.loading_container).setVisibility(View.VISIBLE);
        findViewById(R.id.userverify_card).setVisibility(View.GONE);

        ((TextView)findViewById(R.id.loading_description)).setText(message);
    }

    @MainThread
    private void showSnackbar(String message) {
        Snackbar.make(findViewById(R.id.coordinator),
                message,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @MainThread
    private void retrieveUserData(String license) {

        displayLoading(getString(R.string.retrieve_user_data_loading));

        UserDataRequest userDataRequest = populateUserDataRequest(license);
        mDACAdapter.retrieveUserData(userDataRequest, new DACListenerInterface() {
            @Override
            public void onSuccess(Status status, Object model) {
                mUserDataResponse = (UserDataResponse) model;
                runOnUiThread(() -> displayVerifiedUserInfo());
            }

            @Override
            public void onError(Status status, DACError error) {
                mUserDataResponse = null;
                runOnUiThread(() -> {
                    displayVerifiedUserInfo();
                    showSnackbar(getString(R.string.network_failure_message));
                });
            }
        });

    }

    @MainThread
    protected void displayVerifiedUserInfo() {
        findViewById(R.id.userverify_card).setVisibility(View.VISIBLE);
        findViewById(R.id.loading_container).setVisibility(View.GONE);

        Button verifyMeButton = (Button) findViewById(R.id.verify_ok);
        verifyMeButton.setOnClickListener((View view) -> finish());

        View userVerifyCard = findViewById(R.id.userverify_card);
        if(mUserDataResponse == null) {
            ((TextView) findViewById(R.id.userverify_json)).setText("No data. Try again later.");
        }else{
            ((TextView) findViewById(R.id.userverify_json)).setText(mUserDataResponse.toString());
        }

    }

    private UserDataRequest populateUserDataRequest(String license) {
        UserDataRequest userDataRequest = new UserDataRequest();

        userDataRequest.setLicense(license);
//        userDataRequest.setIncludeServiceResponses(true);
        return userDataRequest;
    }

}
