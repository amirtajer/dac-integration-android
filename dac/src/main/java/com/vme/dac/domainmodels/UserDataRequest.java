package com.vme.dac.domainmodels;

import java.io.Serializable;

/**
 * Created by Amir Tajer on 3/15/18.
 */

public class UserDataRequest implements Serializable{

    private String license;
    private boolean includeServiceResponses;


    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public boolean isIncludeServiceResponses() {
        return includeServiceResponses;
    }

    public void setIncludeServiceResponses(boolean includeServiceResponses) {
        this.includeServiceResponses = includeServiceResponses;
    }

    @Override
    public String toString() {
        return "UserDataRequest{" +
                "license='" + license + '\'' +
                ", includeServiceResponses=" + includeServiceResponses +
                '}';
    }
}
