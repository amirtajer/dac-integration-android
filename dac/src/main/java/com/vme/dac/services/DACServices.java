package com.vme.dac.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.vme.dac.common.DACError;
import com.vme.dac.common.DACErrors;
import com.vme.dac.common.Status;
import com.vme.dac.domainmodels.UserDataRequest;
import com.vme.dac.domainmodels.UserDataResponse;
import com.vme.dac.listener.DACListenerInterface;
import com.vme.dac.domainmodels.CreateLicenseRequest;
import com.vme.dac.domainmodels.CreateLicenseResponse;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.vme.dac.services.DACServices.ErrorCodes.DAC_SERVICE_ERROR;
import static com.vme.dac.services.DACServices.ErrorCodes.DAC_SERVICE_ERROR_EMPTY_RESPONSE;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public class DACServices extends BaseService {

    private static final String TAG = DACServices.class.getSimpleName();
    private final String licenseRequestURL = "https://ca-dac-levio.demo.verified.me/dac/licenseRequest";
    private final String useLicenseURL = "https://ca-dac-levio.demo.verified.me/dac/useLicense" ;

    public final class ErrorCodes {
        public static final String DAC_SERVICE_ERROR = "DAC_SERVICE_ERROR";
        public static final String DAC_SERVICE_ERROR_EMPTY_RESPONSE = "DAC_SERVICE_ERROR_EMPTY_RESPONSE";
    }

    public DACServices(Context context) {
        super(context);
    }

    public void createLicense(final CreateLicenseRequest createLicenseRequest, final DACListenerInterface listener){

        Log.d(TAG, "createLicense started..");

        VolleyLog.DEBUG = true;

        final Map<String, String> mHeaders = new HashMap<>();
        mHeaders.put("Content-Type", "application/json");

        Gson gson = new Gson();
        String jsonString = gson.toJson(createLicenseRequest);

        try {
            JSONObject requestObject = new JSONObject(jsonString);
            RequestQueue queue = Volley.newRequestQueue(context);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest (
                    Request.Method.POST,
                    licenseRequestURL,
                    requestObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response == null) {
                                Log.d(TAG, "response received is empty or blank, abort..");
                                listener.onError(Status.ERROR, new DACError(DAC_SERVICE_ERROR_EMPTY_RESPONSE, DACErrors.EMPTY_RESPONSE));
                                return;
                            }
                            try {
                                CreateLicenseResponse model = new Gson().fromJson(response.toString(), CreateLicenseResponse.class);
                                listener.onSuccess(Status.SUCCESS, model);
                            }catch (Exception e){
                                listener.onError(Status.ERROR, new DACError(DAC_SERVICE_ERROR, DACErrors.EMPTY_RESPONSE));
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //TODO: add more detailed error processing
                            Log.d(TAG, "createLicense got Error!.." + error.getMessage());
                            listener.onError(Status.ERROR, new DACError(DAC_SERVICE_ERROR, null));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return mHeaders;
                }
            };

            queue.add(jsObjRequest);

        } catch (Exception e) {
            Log.e("DAC Exception", e.toString());
        }
    }

    public void retrieveUserData(final UserDataRequest userDataRequest, final DACListenerInterface listener){

        Log.d(TAG, "retrieveUserData started..");

        VolleyLog.DEBUG = true;

        final Map<String, String> mHeaders = new HashMap<>();
        mHeaders.put("Content-Type", "application/json");

        Gson gson = new Gson();
        String jsonString = gson.toJson(userDataRequest);

        try {
            JSONObject requestObject = new JSONObject(jsonString);
            RequestQueue queue = Volley.newRequestQueue(context);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest (
                    Request.Method.POST,
                    useLicenseURL,
                    requestObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response == null) {
                                Log.d(TAG, "response received is empty or blank, abort..");
                                listener.onError(Status.ERROR, new DACError(DAC_SERVICE_ERROR_EMPTY_RESPONSE, DACErrors.EMPTY_RESPONSE));
                                return;
                            }
                            try {
                                UserDataResponse model = new Gson().fromJson(response.toString(), UserDataResponse.class);
                                listener.onSuccess(Status.SUCCESS, model);
                            }catch (Exception e){
                                listener.onError(Status.ERROR, new DACError(DAC_SERVICE_ERROR, DACErrors.EMPTY_RESPONSE));
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //TODO: add more detailed error processing
                            Log.d(TAG, "createLicense got Error!.." + error.getMessage());
                            listener.onError(Status.ERROR, new DACError(DAC_SERVICE_ERROR, null));
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return mHeaders;
                }
            };

            queue.add(jsObjRequest);

        } catch (Exception e) {
            Log.e("DAC Exception", e.toString());
        }
    }
}
