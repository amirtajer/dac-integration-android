package com.vme.dac.domainmodels;

/**
 * Created by Amir Tajer on 3/14/18.
 */
public class DAType {

    private String type;
    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DAType{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
