package com.vme.dac.services;

import android.content.Context;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;

import com.vme.dac.domainmodels.UserDataRequest;
import com.vme.dac.listener.DACListenerInterface;
import com.vme.dac.domainmodels.CreateLicenseRequest;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public class DACAdapter extends BaseService implements DACServiceAPI {

    private static final AtomicReference<WeakReference<DACAdapter>> INSTANCE_REF =
            new AtomicReference<>(new WeakReference<DACAdapter>(null));

    private DACServices dacServices;

    @AnyThread
    protected DACAdapter(Context context) {
        super(context);
        this.dacServices = new DACServices(context);
    }

    @AnyThread
    public static DACAdapter getInstance(@NonNull Context context) {
        DACAdapter dacAdapter = INSTANCE_REF.get().get();
        if (dacAdapter == null) {
            dacAdapter = new DACAdapter(context.getApplicationContext());
            INSTANCE_REF.set(new WeakReference<>(dacAdapter));
        }
        return dacAdapter;
    }

    @Override
    public void createLicense(CreateLicenseRequest createLicenseRequest, DACListenerInterface listener) {
        dacServices.createLicense(createLicenseRequest, listener);
    }

    @Override
    public void retrieveUserData(UserDataRequest userDataRequest, DACListenerInterface listener) {
        dacServices.retrieveUserData(userDataRequest, listener);
    }
}
