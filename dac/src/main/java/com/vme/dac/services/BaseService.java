package com.vme.dac.services;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir Tajer on 3/13/2018.
 */

public class BaseService {
    public Context context;

    public BaseService(Context context) {
        this.context = context;
    }

    /*
      Helper method to extract error (JSON) from VolleyError
    */
    protected JSONObject extractError(VolleyError vError) {
        JSONObject json = null;
        NetworkResponse response = vError.networkResponse;
        if (response != null && response.data != null) {
            String s = new String(response.data);
            try {
                json = new JSONObject(s);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return json;
    }

    /*
    Small helper method to avoid catching the JSON exceptions everywhere in the requests
     */
    protected String getFromJson(JSONObject json, String key) {
        String trimmedString = null;
        try {
            if (json != null) trimmedString = json.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

}
