package com.vme.dac.domainmodels;

import java.util.Map;

/**
 * Created by Amir Tajer on 3/14/18.
 */
public class Asset {

    private String name;
    private Map<String, Object> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Asset{" +
                "name='" + name + '\'' +
                ", data=" + data +
                '}';
    }
}
