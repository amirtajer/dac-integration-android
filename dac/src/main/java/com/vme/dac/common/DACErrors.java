package com.vme.dac.common;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public enum DACErrors {

    NOT_LOGGED_IN,
    BAD_REQUEST,
    INTERNAL_SERVER_ERROR,
    EMPTY_RESPONSE
}
