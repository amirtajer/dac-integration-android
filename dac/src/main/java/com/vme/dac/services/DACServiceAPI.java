package com.vme.dac.services;

import com.vme.dac.domainmodels.UserDataRequest;
import com.vme.dac.listener.DACListenerInterface;
import com.vme.dac.domainmodels.CreateLicenseRequest;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public interface DACServiceAPI {
    void createLicense(CreateLicenseRequest createLicenseRequest, DACListenerInterface listener);
    void retrieveUserData(UserDataRequest userDataRequest, DACListenerInterface listener);
}
