package com.vme.dac.domainmodels;

/**
 * Created by Amir Tajer on 3/13/18.
 */
public class DisplayText {

    private String en;
    private String fr;

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    @Override
    public String toString() {
        return "DisplayText{" +
                "en='" + en + '\'' +
                ", fr='" + fr + '\'' +
                '}';
    }
}
