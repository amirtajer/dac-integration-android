package com.vme.dac.domainmodels;

import java.util.List;

/**
 * Created by Amir Tajer on 3/14/18.
 */
public class Expression {

    private String type;
    private String name;
    private String serviceName;
    private boolean optional;
    private List<String> fields;
    private DAType fromDAType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public DAType getFromDAType() {
        return fromDAType;
    }

    public void setFromDAType(DAType fromDAType) {
        this.fromDAType = fromDAType;
    }

    @Override
    public String toString() {
        return "Expression{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", optional=" + optional +
                ", fields=" + fields +
                ", fromDAType=" + fromDAType +
                '}';
    }
}
