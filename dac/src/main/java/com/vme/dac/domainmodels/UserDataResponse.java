package com.vme.dac.domainmodels;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amir Tajer on 3/13/18.
 */

public class UserDataResponse implements Serializable{

    private String id;
    private String state;
    private List<Asset> assets;
    private List<ServiceResponse> serviceResponses;
    private Auth auth;
    private DAError error;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    public List<ServiceResponse> getServiceResponses() {
        return serviceResponses;
    }

    public void setServiceResponses(List<ServiceResponse> serviceResponses) {
        this.serviceResponses = serviceResponses;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public DAError getError() {
        return error;
    }

    public void setError(DAError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "UserDataResponse{" +
                "id='" + id + '\'' +
                ", state='" + state + '\'' +
                ", assets=" + assets +
                ", serviceResponses=" + serviceResponses +
                ", auth=" + auth +
                ", error=" + error +
                '}';
    }
}
